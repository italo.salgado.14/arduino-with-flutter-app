/*   
HC05 - Bluetooth AT-Command mode  
modified on 10 Feb 2019 
by Saeed Hosseini 
https://electropeak.com/learn/ 
*/ 
#include <Servo.h>
#include <SoftwareSerial.h> 

/*INICIALIZACION DE VARIABLES*/
SoftwareSerial MyBlue(0, 1); // RX | TX, esto define que pines ocupar
String readString;
char posX;
char posY;
int pos = 0;  // variable to store the servo position
boolean flag1 = false;
boolean flag2 =false;


Servo myservo;  // cheate servo object to control a servo
 
void setup() 
{   
  myservo.attach(10); // attaches the servo on pin 10 to the servo object  
  
  Serial.begin(9600);  //baudrate
  Serial.println("Ready to connect\nDefualt password is 1234 or 000"); 
} 
void loop() 
{ 
  /* */
  while (Serial.available()) {
    delay(10);  //small delay to allow input buffer to fill

    char c = Serial.read();  //gets one byte from serial buffer
    if (c == 'x'){
      flag1 = true;
      break;
    }
    if (flag1 ){
        posX = c;
        Serial.print("x :");
        Serial.println(posX);
        flag2 = true; 
        break;  
    }
    if (flag2){
        posY = c;
        Serial.print("y :");
        Serial.println(posY); 
        flag1 = false; 
        flag2 = false;  
    }
    
    if (c == ',') {
      
      break;
    }  //breaks out of capture loop to print readstring
    //readString += c;
  } //makes the string readString 

 /* if (readString.length() >0) {  
    //PosX = getValue(readString,'x',0);
    //PosY = getValue(readString,'y',0);


    delay(10);  //small delay to allow input buffer to fill
  // Serial.println(readString);   
     /* SERVO FASE 
    // sweeps from 0 degrees to 180 degrees
    for(pos = 0; pos <= 180; pos += 1) 
    {
      myservo.write(pos);
      delay(30);
    }
    // sweeps from 180 degrees to 0 degrees
    for(pos = 180; pos>=0; pos-=1)
    {
      myservo.write(pos);
      delay(30);
    } 
    readString=""; //clears variable for new input
    
  }*/
} 

/*
String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
*/
